<?php

try
{
// On se connecte à MySQL

$bdd = new PDO('mysql:host=localhost;dbname=GIP_schema;charset=utf8', 'root', 'userpop');

}
catch(Exception $e)
{

// En cas d'erreur, on affiche un message et on arrête tout
die('Erreur : '.$e->getMessage());
}

$id = $bdd->prepare("SELECT COUNT(*) FROM users WHERE pseudo = ? OR password = ?");
$id->execute(array($_POST['pseudo'], password_hash($_POST['passwordOne'], PASSWORD_BCRYPT)));

if ($id->connect_error) {
    die('Erreur de connexion : ' . $mysqli->connect_error);
}
 ?>
